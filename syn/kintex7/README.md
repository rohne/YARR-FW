# Synthesis Setup

The folder structure was designed to compile different firmware versions for all permutations of:
- Hardware (e.g. TEF1001-R2)
- Configuration (e.g. 16 channels each 1 lane, no trigger interface [16x1])

In order to generate a Makefile or Vivado Project use hdlmake, install via
```
$ pip install hdlmake
```

## Top Module

The top module is ``bram_trenz.vhd``, it splits the firmware in two pieces

1. The PCIe interface
2. The "application" ``app.vhd``

The application carries the custom firmware.

This split was performed to support tandem configuration (on-the-fly reconfiguration of the application part of the FPGA).

## Folder Structure

All folders have to follow the following format:
```
<fw_configuration>
 |__ <hardware0>
     |__ Manifest.py
 |__ <hardware1>
     |__ Manifest.py
 |__ board_pkg.vhd
```

The ``Manifest.py`` can point to specific constraint files required for the hardware platform. The ``board_pkg.vhd`` contains the custom configuration for this firmware.


